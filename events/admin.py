from django.contrib import admin
from .models import EventAnnouncement, EventTimeVote

# Register your models here.
admin.site.register(EventAnnouncement)
admin.site.register(EventTimeVote)
from django.db import models
from django.contrib.auth.models import User
from propositions.models import Category
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.

class EventAnnouncement(models.Model):
    name = models.TextField()
    duration = models.FloatField()
    created = models.DateTimeField(auto_now_add=True)
    allowed_categories = models.ManyToManyField(Category)
    allowed_start = models.DateTimeField()
    allowed_end = models.DateTimeField()
    
    def __str__(self):
        return self.name

    
class EventTimeVote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    event = models.ForeignKey(EventAnnouncement, on_delete=models.CASCADE)
    
    def __str__(self) -> str:
        return str(self.created)
    
    
class CategoryVote(models.Model):
    strength = models.FloatField(validators=[MinValueValidator(-1.0), MaxValueValidator(1.0)])
    category = models.OneToOneField(Category, on_delete=models.CASCADE)
    vote = models.ForeignKey(EventTimeVote, on_delete=models.CASCADE)
    

class TimeSlot(models.Model):
    start = models.DateTimeField()
    end = models.DateTimeField()
    vote = models.ForeignKey(EventTimeVote, on_delete=models.CASCADE)
    
from rest_framework import serializers
from profile.serializers import ProfileSerializerAdmin

from propositions.serializers import CategorySerializer, Category
from .models import EventAnnouncement, EventTimeVote, TimeSlot, CategoryVote
from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework.validators import UniqueTogetherValidator
        
class CategoryVoteSerializer(serializers.ModelSerializer):
    category = serializers.PrimaryKeyRelatedField(queryset=Category.objects.all())

    class Meta:
        model = CategoryVote
        exclude = ['vote']
        
class TimeSlotSerializer(serializers.ModelSerializer):

    class Meta:
        model = TimeSlot
        exclude = ['vote']  
        
class EventTimeVoteSerializer(WritableNestedModelSerializer):
    timeslot_set = TimeSlotSerializer(many = True, default = [])
    categoryvote_set = CategoryVoteSerializer(many = True, default = [])

    class Meta:
        model = EventTimeVote
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(
                queryset=EventTimeVote.objects.all(),
                fields=['user', 'event']
            )
        ]
        

class EventAnnouncementSerializer(serializers.ModelSerializer):
    vote_set = EventTimeVoteSerializer(many = True, read_only = True, default = [])
    allowed_categories = CategorySerializer(many = True, read_only = True)
    allowed_categories_id = serializers.PrimaryKeyRelatedField(many = True, 
                                                               queryset=Category.objects.all(),
                                                               write_only = True,
                                                               source = 'allowed_categories')


    class Meta:
        model = EventAnnouncement
        fields = '__all__'

from django.urls import include, path
from .views import (EventAnnouncementList, EventAnnouncementUpdate, 
                    EventTimeVoteList, EventTimeVoteUpdate
                    )


urlpatterns = [
    path('', EventAnnouncementList.as_view()),
    path('<int:pk>', EventAnnouncementUpdate.as_view()),
    path('vote/<int:pk>', EventTimeVoteUpdate.as_view()),
    path('vote/', EventTimeVoteList.as_view()),
]
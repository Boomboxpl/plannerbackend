from django.shortcuts import render
from rest_framework import generics

from .models import EventAnnouncement, EventTimeVote
from planner.permissions import OwnerPermission, ReadOnly
from rest_framework.permissions import IsAdminUser
from .serializers import EventAnnouncementSerializer, EventTimeVoteSerializer

# Create your views here.
class EventAnnouncementList(generics.ListCreateAPIView):
    queryset = EventAnnouncement.objects.all()
    serializer_class = EventAnnouncementSerializer
    permission_classes = [OwnerPermission | IsAdminUser | ReadOnly]
    ordering = ['-created']
    
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
    

class EventAnnouncementUpdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = EventAnnouncement.objects.select_related('allowed_categories')
    permission_classes = [OwnerPermission | IsAdminUser]
    serializer_class = EventAnnouncementSerializer
    
    
class EventTimeVoteList(generics.ListCreateAPIView):
    queryset = EventTimeVote.objects.all()
    permission_classes = [OwnerPermission | IsAdminUser | ReadOnly]
    serializer_class = EventTimeVoteSerializer
    
    
class EventTimeVoteUpdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = EventTimeVote.objects.all()
    permission_classes = [OwnerPermission | IsAdminUser]
    serializer_class = EventTimeVoteSerializer
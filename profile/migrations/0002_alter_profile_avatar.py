# Generated by Django 4.1.3 on 2022-11-10 17:48

import profile.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='avatar',
            field=models.ImageField(default='profile_images/defaultProfile.jpg', upload_to=profile.models.user_directory_path),
        ),
    ]

import os
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

def user_directory_path(instance, filename):
    file, ext = os.path.splitext(filename)
    return 'profile_images/user_{0}'.format(instance.user.pk)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_blocked = models.BooleanField(default = False)
    is_developer = models.BooleanField(default = False)
    avatar = models.ImageField(default='profile_images/defaultProfile.jpg', upload_to=user_directory_path)
    bio = models.TextField(blank = True)
    
    def __str__(self):
        return self.user.username

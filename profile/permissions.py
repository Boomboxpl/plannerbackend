from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated

class UnblockedPermission(permissions.BasePermission):
    message = "You're blocked. Contact administrator."
    
    def has_permission(self, request, view):
        return not request.user.profile.isBlocked
    
class DeveloperPermission(permissions.BasePermission):
    message = "You're not game developer. Contact administrator."
    
    def has_permission(self, request, view):
        return request.user.profile.isDeveloper or request.user.is_staff
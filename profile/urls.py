from django.urls import include, path
from .views import ProfileCurrentDetail, UserCreate, ProfileList, ProfileDetail, ProfileUpdate, ProfileUpdateAdmin, UserUpdateAdmin


urlpatterns = [
    path('register/', UserCreate.as_view(), name='create-profile'),
    path('', ProfileList.as_view()),
    path('<int:userId>/', ProfileDetail.as_view(), name='retrieve-profile'),
    path('current/', ProfileCurrentDetail.as_view(), name='retrieve-profile'),
    path('update/<int:userId>/', ProfileUpdateAdmin.as_view(), name='update-profile'),
    path('update/current/', ProfileUpdate.as_view(), name='update-profile')
]
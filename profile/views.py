from django.shortcuts import get_object_or_404, render
from rest_framework import generics, permissions
from .serializers import ProfileSerializer, UserCreateSerializer, ProfileSerializerAdmin, UserModel
from .models import Profile
from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAdminUser, IsAuthenticated

# Create your views here.

class ProfileList(generics.ListAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializerAdmin
    ordering = ['-user__pk']
    
class ProfileDetail(generics.RetrieveAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializerAdmin
    lookup_field = 'user'
    lookup_url_kwarg = 'userId'
    
class ProfileCurrentDetail(generics.RetrieveAPIView):
    serializer_class = ProfileSerializerAdmin
    permission_classes = [IsAuthenticated]
    
    def get_queryset(self):
        return Profile.objects.filter(user = self.request.user)
    
    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset)
        return obj
    
class ProfileUpdate(generics.UpdateAPIView):
    # API endpoint that allows a Profile record to be updated.
    
    def get_queryset(self):
        return Profile.objects.filter(user = self.request.user)
    
    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset)
        return obj
    
    serializer_class = ProfileSerializer
    
class ProfileUpdateAdmin(generics.UpdateAPIView):
    permission_classes = [IsAdminUser]
    lookup_field = 'user'
    lookup_url_kwarg = 'userId'
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializerAdmin
    
    def get_object(self):
        queryset = self.get_queryset()
        owner = UserModel.objects.get(pk = self.kwargs[self.lookup_url_kwarg])
        obj = get_object_or_404(queryset, user = owner)
        return obj
    
    
class UserCreate(generics.CreateAPIView):
    model = get_user_model()
    permission_classes = [permissions.AllowAny]
    serializer_class = UserCreateSerializer
    
class UserUpdateAdmin(generics.UpdateAPIView):
    model = get_user_model()
    permission_classes = [IsAdminUser]
    lookup_field = 'user'
    lookup_url_kwarg = 'userId'
    serializer_class = UserCreateSerializer


from django.contrib import admin

from .models import Category, Proposition, PropositionVote

# Register your models here.

admin.site.register(Category)
admin.site.register(Proposition)
admin.site.register(PropositionVote)
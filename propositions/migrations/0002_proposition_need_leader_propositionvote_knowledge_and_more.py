# Generated by Django 4.2.6 on 2023-10-22 11:52

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('propositions', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='proposition',
            name='need_leader',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='propositionvote',
            name='knowledge',
            field=models.FloatField(null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1.0)]),
        ),
        migrations.AddField(
            model_name='propositionvote',
            name='like',
            field=models.FloatField(default=0, validators=[django.core.validators.MinValueValidator(-1.0), django.core.validators.MaxValueValidator(1.0)]),
        ),
    ]

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models import Avg, Max

# Create your models here.

class Category(models.Model):
    name = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    
    def __str__(self) -> str:
        return self.name

class Proposition(models.Model):
    name = models.TextField()
    description = models.TextField()
    category = models.ForeignKey(Category, null=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True)
    need_leader = models.BooleanField(default=False)
    average_vote = models.FloatField(null= True, blank=True)
    max_knowledge = models.FloatField(null= True, blank=True)
    
    def get_average_vote(self):
        try:
            return self.propositionvote_set.aggregate(Avg('like'))['like__avg']
        except:
            return None
    
    def get_max_knowledge(self):
        try:
            return self.propositionvote_set.aggregate(Max('knowledge'))['knowledge__max']
        except:
            return None
    
    def save(self, *args, **kwargs):
        self.average_vote = self.get_average_vote()
        self.max_knowledge = self.get_max_knowledge()
        super(Proposition, self).save(*args, **kwargs)
    
    def __str__(self) -> str:
        return self.name
    
class PropositionVote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    proposition = models.ForeignKey(Proposition, on_delete=models.CASCADE)
    like = models.FloatField(validators=[MinValueValidator(-1.0), MaxValueValidator(1.0)], default=0)
    knowledge = models.FloatField(validators=[MinValueValidator(0), MaxValueValidator(1.0)], null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    
    def save(self, *args, **kwargs):
        super(PropositionVote, self).save(*args, **kwargs)
        self.proposition.save()
    
    def __str__(self) -> str:
        return str(self.created)
from rest_framework import serializers
from .models import Category, Proposition, PropositionVote
from rest_framework.validators import UniqueTogetherValidator
from profile.serializers import ProfileSerializerAdmin

class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'
        
        
class PropositionVoteSerializer(serializers.ModelSerializer):

    class Meta:
        model = PropositionVote
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(
                queryset=PropositionVote.objects.all(),
                fields=['user', 'proposition']
            )
        ]
        
        
class PropositionSerializer(serializers.ModelSerializer):
    propositionvote_set = PropositionVoteSerializer(many = True, read_only = True, default = [])

    class Meta:
        model = Proposition
        fields = '__all__'
        read_only_fields = ['average_vote', 'max_knowledge']

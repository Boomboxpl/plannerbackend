from django.urls import include, path
from .views import (CategoryList, CategoryUpdate,
                    PropositionList, PropositionUpdate,
                    PropositionVoteList, PropositionVoteUpdate)


urlpatterns = [
    path('category/', CategoryList.as_view()),
    path('category/<int:pk>', CategoryUpdate.as_view()),
    path('', PropositionList.as_view()),
    path('<int:pk>', PropositionUpdate.as_view()),
    path('vote/', PropositionVoteList.as_view()),
    path('vote/<int:pk>', PropositionVoteUpdate.as_view())
]
from django.shortcuts import render

from rest_framework import generics
from .models import Category, Proposition, PropositionVote
from .serializers import CategorySerializer, PropositionSerializer, PropositionVoteSerializer
from planner.permissions import OwnerPermission, ReadOnly
from rest_framework.permissions import IsAdminUser

class CategoryList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [OwnerPermission | IsAdminUser | ReadOnly]
    ordering = ['-created']
    
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)

class CategoryUpdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    permission_classes = [OwnerPermission | IsAdminUser]
    serializer_class = CategorySerializer
    
    
class PropositionList(generics.ListCreateAPIView):
    queryset = Proposition.objects.select_related('user', 'category')
    serializer_class = PropositionSerializer
    permission_classes = [OwnerPermission | IsAdminUser | ReadOnly]
    ordering_fields= ['created', 'average_vote', 'max_knowledge', 'name']
    ordering = ['-created']
    filterset_fields = {
        'category' : ['exact'],
        'user' : ['exact'],
    }
    
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)


class PropositionUpdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = Proposition.objects.select_related('user', 'category')
    permission_classes = [OwnerPermission | IsAdminUser]
    serializer_class = PropositionSerializer


class PropositionVoteList(generics.ListCreateAPIView):
    queryset = PropositionVote.objects.select_related('user', 'proposition')
    serializer_class = PropositionVoteSerializer
    permission_classes = [OwnerPermission | IsAdminUser | ReadOnly]
    ordering = ['-created']
    filterset_fields = {
        'user' : ['exact'],
        'proposition' : ['exact']
    }
    
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
    

class PropositionVoteUpdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = PropositionVote.objects.select_related('user', 'proposition')
    permission_classes = [OwnerPermission | IsAdminUser]
    serializer_class = PropositionVoteSerializer